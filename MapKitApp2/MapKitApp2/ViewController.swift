//
//  ViewController.swift
//  MapKitApp2
//
//  Created by AlexM on 18/12/14.
//  Copyright (c) 2014 AlexM. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController, MKMapViewDelegate {
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var textField: UITextField!
    var myRoute : MKRoute?

    @IBAction func searchButton(sender: AnyObject) {
        //BUSCAR CIUDAD
        var address = self.textField.text
        var geocoder = CLGeocoder()
        
        //Aqui se busca la ciudad introducida por el usuario y hacemos zoom
        geocoder.geocodeAddressString(address, {(placemarks: [AnyObject]!, error: NSError!) -> Void in
            if let placemark = placemarks?[0] as? CLPlacemark {
                self.mapView.addAnnotation(MKPlacemark(placemark: placemark))
                
                //Definimos el tamaño del área a mostrar: 1 equivale a 111km.
                //El primer número indica distancia de Norte a Sur, y el segundo de Este a Oeste.
                var span = MKCoordinateSpanMake(2, 2)
                var region = MKCoordinateRegion(center: placemark.location.coordinate, span: span)
                self.mapView.setRegion(region, animated: true)
            }
        })
    }
    
    //CREAR RUTA
    func mapView(mapView: MKMapView!, rendererForOverlay overlay: MKOverlay!) -> MKOverlayRenderer! {
            var myLineRenderer = MKPolylineRenderer(polyline: myRoute?.polyline!)
            myLineRenderer.strokeColor = UIColor.redColor()
            myLineRenderer.lineWidth = 3
            return myLineRenderer
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

